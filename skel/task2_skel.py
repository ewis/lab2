import time

def filter_words(text, stopwords):
    filtered_text = []
    for word in text:
        if word not in stopwords:
            filtered_text.append(word)
    return filtered_text

stopwords = [
 'a', 'an', 'and', 'are', 'as', 'at', 'be', 'by',
 'for', 'from', 'has', 'he', 'in', 'is', 'it', 'its',
 'of', 'on', 'that', 'the', 'to', 'was', 'were', 'will', 'with'
]

text = "Mr. Hat is feeding the black cat."

words = text.split(" ")
print("words: ", words)

# filter using list
filtered = filter_words(words, stopwords)
print(filtered)

# filter using set
stopwords_set = set(stopwords)
filtered = filter_words(words, stopwords_set)
print(filtered)

# filter using dict
stopwords_dict = dict.fromkeys(stopwords, True)
filtered = filter_words(words, stopwords_dict)
print(filtered)

# TODO: compare the performance of word filtering in the proposed scenarios
