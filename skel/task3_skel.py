
import numpy as np
import matplotlib.pyplot as plt

def create_dictionary(data, init, normalized):
    if init is not None:
        s = init
        dictionary = dict.fromkeys(s, 0)
    else:
        dictionary = {}

    for x in set(data):
        if normalized:
            dictionary[x] = data.count(x) / len(data)
        else:
            dictionary[x] = data.count(x)
    return dictionary

def print_bar_chart(d):
    # plt.bar(d.keys(), d.values(), width=0.5, color='g')
    # plt.xticks(range(len(d.keys())))
    # plt.show()
    plt.bar(range(len(d)), d.values(), width=0.5, color='g')
    plt.xticks(range(len(d)), list(d.keys()))
    plt.show()

with open('text.txt') as f:
    text = f.read()
    text = text.lower()
    d = create_dictionary(text, 'abcdefghijklmnopqrstuvwxyz', False)
    print_bar_chart(d)