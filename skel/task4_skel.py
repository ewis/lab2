
import numpy as np
import matplotlib.pyplot as plt

def create_dictionary(data):
    dictionary = {}
    for x in set(data):
        dictionary[x] = data.count(x) / len(data)
    return dictionary

def histogram_rand():
    n_data = 10000
    mu = 100
    sigma = 15

    data = mu + sigma * np.random.randn(n_data)
    data = [int(d) for d in data]

    plt.hist(data, normed=True, bins=30)
    plt.xlabel('Values')
    plt.ylabel('Probability')
    plt.show()

histogram_rand()